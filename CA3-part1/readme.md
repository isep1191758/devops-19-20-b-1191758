Duarte Rothes Barbosa
1191758@isep.ipp.pt

# Class Assignment 3, Part 1 - Class B, Group 5

27-04-2020


  Goals/Requirements
  ------------------

  * Main Goal:

  Practice with VirtualBox using the same projects from the previous assignments but now inside a VirtualBox VM with Ubuntu

  * Requirements:
  
  1. Create a VM as described in the lecture

  2. Clone my individual repository inside the VM

  3. To build and execute all the projects from the previous assignments 
  (spring boot tutorial basic project and the gradle_basic_demo project)

  4. For web projects, access the web applications from the browser in the host machine
   
  5. For projects such as the simple chat application, execute the server inside the VM and the clients in the host machine

  6. Describe the process in the readme file

  7. Mark the repository with the tag CA3-part1


# A. Get started


  The first step was to create a new folder for Class Assignment 3 and a subfolder for part1 and then commit it to the repository with:

    ->   git add CA3-part1/
    ->   git commit -m "CA3-part1 folder creation"
    ->   git push



# B. Create the VM


Created a new VM on VirtualBox with the settings given in the class slides.


In the main menu of VirtualBox, at File> Host Network Manager, new host-only network (vboxnet0) has been       created with the following IP address: 192.168.56.1


On the Settings button of the VM, on the Network tab, clicked the Adapter1 subtab and NAT has been set as the first network adapter of the VM. The second one was set to be the vboxnet0 host-only adapter, previously created.




# C. Starting the VM and Ubuntu installation


After initializing the previously created virtual machine, the next task is to start the command-line installation of Ubuntu.


The installation finished and the mini.iso disk, used to install Ubuntu on the VM, was ejected.


Then, Ubuntu has been run and logged in.



# D. System configuration


Next, it has been installed the network tools with:

    ->   sudo apt install net-tools


To edit the network configuration file to match the one in the class slides it has been used the following command:


    ->   sudo nano /etc/netplan/01-netcfg.yaml


The settings written in the file were:

    network:
      version: 2
      renderer: network
     ethernets:
         enp0s3:
             dhcp4: yes
         enp0s8:
             addresses:
                 - 192.168.56.5/24
                                

To apply the changes the following command was writen:


    ->   sudo netplan apply'

To be able to use the SSH to open secure terminal sessions on the VM from other hosts, the openssh-server was installed with:


    ->   sudo apt install openssh-server


Then, to install an FTP server to transfer files to/from the VM, the following command was run:

    ->   sudo apt install vsftpd

To grant writing access to vsftpd the following steps were followed:


   1. run `sudo nano /etc/vsftpd.conf` to edit the configuration file of vsftpd
   2. uncomment `write_enable=YES`
   3. run `sudo service vsftpd restart`



Next, in the terminal of the host computer, it has been typed:

    ->   ssh drb8037rp@192.168.56.5


To install Git in the VM, it has been run:

    ->   sudo apt install git


And to install Java:


    ->   sudo apt install openjdk-8-jdk-headless




# E. Cloning the repository




To clone my repository to the VM, the following command was typed:

    ->   git clone https://isep1191758@bitbucket.org/isep1191758/devops-19-20-b-1191758.git




# F. Building and Executing Projects



This command was run inside the project folder in the VM to install Maven:

    ->   sudo apt-get install maven


The first command to execute the application `mvn spring-boot:run`, was to execute the React.js and Spring Data REST, by typing in Chrome browser 'http://localhost:8080/', appearing the table issued in CA1.


To run the Chat Server App, and after cloning my original repository in VM, I've typed the following also in VM:

    ->   gradle clean build


And after build successfully, I've runned:

    ->   java -cp build/libs/basic_demo-0.1.0.jar basic_demo.ChatServerApp 59001


wich appeared the message: 'The chat server is running...'


By the fact VM does not have a GUI (Graphical User Interface), for appearing other terminals, I run the program simultaneosly on IDE typing 'gradlew runClient', and it run beautifully.




# G. Final Tag


To finish the assignment, a new tag was added to the repository: `

    ->   git tag ca3-part1`
    ->   git push origin ca3-part1`.
