Duarte Rothes Barbosa,
1191758@isep.ipp.pt

#  Class Assignment 4, Turma B, Grupo 5

13-05-2020
 
##  Demonstração através de Docker Compose VM
 
 
# Objetivos/Requisitos
 
 
 
# Objetivo Principal:

Usar a ferramenta Docker Toolbox e Oracle VM VirtualBox, para efetuar as mesmas operações de exercícios anteriores: Spring Boot Tutorial Rest (class Employee) + Construção DB em H2.
 
 
# Requisitos:

1.	Instalar o programa Docker Toolbox (no meu caso devido à versão OS Windows 10 Home no meu PC, e consequente incompatibilidade com o Docker Desktop), através de             https://docs.docker.com/toolbox/overview/, tendo ficado instalada a versão 19.03.1, build 74b1e89e8a


 
2.	Os dois Dockerfile (web e db) bem como o ficheiro docker-compose.yml, são colocados na pasta deste assignement (CA4)
 
3.	Na continuação, e para os projetos web, aceder ao browser do Host para verificar se os dois projetos "correm" após as mudanças necessárias nos Dockerfiles, aparecendo a clássica tabela Employee com 3 atributos na versão Web, e a consola da DB para introdução de novos dados (H2-console)
 
4.	Efetuar a publicação das imagens geradas dos containers web e db, criando um repositório para o efeito em https://hub.docker.com
 
5.	Criar um ficheiro *.db para que o container da base de dados possa ser sempre carregado ao ser "solicitado", sendo o mesmo colocado na pasta *data*, dentro da pasta deste assignement
 
6.	Efetuar uma tag com a designação 'CA4(final)', na parte final
 
7.	Descrever o processo tomado num ficheiro readme.md.
 
 
# A. Início
 
 O primeiro passo é criar uma pasta nova para este Assignement com o nome CA4, localizada na raiz, e efetuar um commit através de:

    ->   git add CA4
    ->   git commit -m "CA4 creation and Clone docker-compose-spring-tut-demo Repo"
    ->   git push
 

 
# B. Instalação e Adaptação dos ficheiros Docker
 
1.	Primeiramente, fazer o download dos componentes Docker através de https://isep1191758@bitbucket.org/atb/docker-compose-spring-tut-demo.git, e colocá-lo na pasta criada para o efeito CA4;
 
2.	No ficheiro db/Dockerfile, efetuei a seguinte adaptação/acrescento/substituição:

	    RUN git clone https://isep1191758@bitbucket.org/isep1191758/devops-19-20-b-1191758.git

	    WORKDIR /tmp/build/devops-19-20-b-1191758/CA2-part2

	    RUN chmod a+x ./gradlew
	     
  	    RUN chmod 777 ./node_modules/.bin/webpack
                                               
        RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/

e seguidamente, gravei as alterações em diversos *commits*;

3.	Deste modo, e apenas com a alteração do ficheiro *Dockerfile* constante na pasta *db*, o repositório ficou preparado para executar o Docker Compose.



# C. Operações Docker-Compose (build & up)


1.	Após efetuar start no Oracle VM VirtualBox Manager, liguei a VM entretanto gerada pela instalação do Docker Toolbox;
2.	Iniciei uma sessão no Docker Quickstart Terminal (run as Administrator);

3.	Efetuar o seguinte comando no Docker, após direcionar a root para a pasta de trabalho onde consta o ficheiro docker-compose.yml:
C:\Users\ASUS\IdeaProjects\devops-19-20-b-1191758\CA4
4.     $ docker-compose build
5.	Seguidamente efetuar:
6.     $ docker-compose up
7. Este comando cria as duas máquinas virtuais (*containers*) quando é pela primeira vez executado (ca4_db_1 e ca4_web_1)
8. Para verificar se os containers foram corretamente criados, e estão os dois corretamente ligados, efetuar o seguinte comando:
9.     $ docker-compose ps
     Se aparecerem os dois nomes e o seu estado for UP, indica que as VM's foram corretamente acionadas.
 
10. Seguidamente e após a verificação, efetuar numa linha de comandos CMD:
11.     / docker-machine ip default 
12. Esse IP deverá ser colocado em substituição no URL de *localhost:*, ie:
De: http://localhost:8080/demo-0.0.1-SNAPSHOT/, passa a http://192.168.99.100:8080/demo-0.0.1-SNAPSHOT/, para a aplicação web Tomcat;
E de http://localhost:8080/demo-0.0.1-SNAPSHOT/h2-console/, passa a http://192.168.99.100:8080/demo-0.0.1-SNAPSHOT/h2-console/, para a consola H2;

13.	Ao abrir estes dois links o resultado será a tabela produzida pelo Spring Basic App criada em CA2-part2, e no segundo a consola H2 database (no URL dever-se-á introduzir ‘jdbc:h2:tcp://192.168.33.11:9092/./jpadb’).



# D. Gravação das imagens Web e Db em hub.docker.com



1. Após ter efetuado o registo em hub.docker.com para obter o meu repositório para a gravação das imagens dos dois containers, digitei na linha de comandos do Docker:

                 $ docker images 
                 
    para listar as imagens geradas pelos containers db e web, tendo obtido o seguinte:
    
    
                 ca4_web                     latest                f583fce56621          25 hours ago           1.68GB
                 ca4_db                          latest              6b24fa1607ac        27 hours ago        285MB
                 
                 

2. Depois de efetuado o login no repositório agora criado (username: 0b775834563a) , efetuo os seguintes comandos para gravação/publicação da imagem do container Web (o do container Db é exatamente igual variando apenas o ID da imagem):


                $ docker tag f583fce56621 0b775834563a/ca4_web:1.0
                $ docker push 0b775834563a/ca4_web
                $ docker pull 0b775834563a/ca4_web:1.0
  
  
  
3. E o resultado foi:


                1.0: Pulling from 0b775834563a/ca4_web
                Digest: sha256:cd9be8b2dfbeb26eb6cf6867877e1ecd6f59a0a53b94890453efdb41ff84f2be
                Status: Image is up to date for 0b775834563a/ca4_web:1.0
                docker.io/0b775834563a/ca4_web:1.0




# E. Criação de um ficheiro *.db dentro da pasta *data* criando a persistência da BD



1. Ainda na linha de comandos Docker, e na root de CA4 (ver ponto 3. do capítulo C), digitei para obter a listagem de containers ativos:
                
                $ docker ps

           tendo obtido a seguinte informação em relação ao db container:
                2c6b351dc73c        ca4_db
                
2. Seguidamente, e para entrar dentro do container, digito:

	            $ docker exec -it 2c6b351dc73c /bin/bash

3. Já dentro do contentor, aparece a seguinte root, já depois de ter efetuado a listagem dos ficheiros nele contidos:

                root@2c6b351dc73c:/usr/src/app# ls
                h2-1.4.200.jar  jpadb.mv.db
         
4. Conforme o ponto 4. dos Goals/Requirements, efetuo a cópia do ficheiro jpadb.mv.db para dentro da pasta *data*, localizada em CA4, através de:

                root@2c6b351dc73c:/usr/src/app# cp jpadb.mv.db /usr/src/data
   
5. E no final se verifica que o ficheiro se encontra mesmo na pasta pretendida:

                root@2c6b351dc73c:/usr/src/data# ls
                jpadb.mv.db  readme.txt
         
6. Finalmente, marcar o meu repositório bitbucket com a TAG:

                git tag CA4(final)
                git push origin master CA4(final).