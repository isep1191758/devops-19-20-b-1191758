Duarte Rothes Barbosa
1191758@isep.ipp.pt

# Class Assignment 1 - Class B, Group 5

26-03-2020

# A. Project Creation
1. Downloaded the content of the project "React.js and Spring Data REST" (RSDR) from
https://github.com/spring-guides/tut-react-and-spring-data-rest, also through the Tutorial available at
https://spring.io/guides/tutorials/react-and-spring-data-rest/

2. Created the project RSDR importing it from the pom.xml project file in IDE


## B. Repository Creation
1. Created a repository in BitBucket named "devops-19-20-b-1191758",

Already in terminal's Intellij IDE, after the import of the project RDSR from the pom.xml, I did:

2. The git's configuration using `git config -l`, and also configurating user.name and user.email fields

3. Typing 'git init' for control start (MY PATH -> C:\Users\ASUS\IdeaProjects\devops-19-20-b-1191758)

4. Start to controll all files in RSDR project using 'git add -A'

5. Initial commit typing 'git commit -m "First Commit"'

6. The connection between project RSDR and my repository through command 'git remote add origin https://isep1191758@bitbucket.org/isep1191758/devops-19-20-b-1191758.git'

7. A tag in the master branch using `git tag -a v1.2.0 -m "MY VERSION 1.2.0"`

8. Push the commit through `git push origin master`

9. Push the tag for v1.2.0 using `git push origin v1.2.0`


## C. Branch Creation
1. Concerning the development of new features, I did:

3. Through command 'git checkout -b email-field', to create a new branch called "email-field" and move the HEAD to this new branch

2. Make the necessary software developments for class Employee, adding new fields and methods of RSDR, making that class to have one more attribute (email)

3. Tests added in class Employee for email-field, creating EmployeeTest.java file

4. Commit the changes through 'git commit' and making them "alive" in project trough 'git push'

5. To tag the commit that included the tests for class Employee using `git tag -a v1.3.0 -m "VERSION 1.3.0 - Employee added email field"`

6. Finally, I've pushed the new tag to the remote repository using `git push origin v1.3.0`


## D. Closing branches and switching HEAD
1. Changed the HEAD to master using 'git checkout master'

2. Did the merge with the Master branch and the email-field branch using `git merge email-field`

3. Pushed the merged version to the remote repository using `git push`

-> Note: I didn't erased the existing branches, so they can appear in Bitbucket repository, same also with tags.


--> I've also created more issues and branches (e.g. "fix-invalid-email" appearing on Repository commit messages), in regarding of CA1_requirements.ppt file, containing tests for invalid email issue in EmployeeTest.java, and for that repeated all steps from point B. to D.,and finally tagged my final Version with v1.3.1.


## Required technology setup
- Java JDK 8 or later
- Maven 3.2+
- React Developer Tools 4.5.0 (Google Chrome plugin)

It can be used with any IDE, like Eclipse or IntelliJ.

## Client and server languages
Client and server parts are written in Java. 

## Debugging
Through the IDE's debugging tool.

## Testing
Tests can be done using JUnit.

## Running the web app
In order to view the result of the work done in the project on a browser, it is necessary to execute the maven command in the path (C:\Users\ASUS\IdeaProjects\devops-19-20-b-1191758\basic), `mvn spring-boot:run` and open the page localhost:8080 (assuming an index.html file exists).

The result is a new table with the new Employee's attribute called "Email", appearing in the right side.

_________________________________________________


### Alternative technological solution (Implementation)


### Creation of a copy project RSDR with an alternative repository (not based on Git, as required):
1. Go to the Mercurial website (https://www.mercurial-scm.org/), and install Mercurial v5.0.2

2. Go to the HelixTeamHub website (https://helixteamhub.cloud/), and create an account, project, and a new repository

3. In your DEVOPS directory, create another named CA1_alt but outside CA1 repository, for having absolute independency and control, and for not creating a new repository inside the Bitbucket repository

4. In new CA1_alt fold, copy all files from CA1, and create a new HelixTeamHub Repository

5. In the same folder, click right and choose "Tortoise Repository Settings" for filling with your username <email> field and click "Ask Username" check box in "true", and finally edit and save correspondent [ui] file

6. Open a new project in Intellij from Version Control option, and clone http from HelixTeamHub Repository to URL's IDE, and choose "Mercurial" option to the repository source

7. From this point, you have the same RSDR project in IDE, with two original different repositories

________________________________________________

#### Git vs. Mercurial


#### A. Main Difference

- The biggest difference between Mercurial vs. Git is the branching structure. It can be argued that branching is better in Git than in Mercurial. Even though Mercurial may be easier to learn and use, its branching model often creates confusion.


#### B. Usability

- When comparing Git vs. Mercurial, one of the biggest differences is the level of expertise needed to use each system. Git is more complex, and it requires your team to know it inside and out before using it safely and effectively. With Git, one sloppy developer can cause major damage for the whole team. Its documentation is also harder to understand.

In general, if your team is less experienced, or if you have non-technical people on your team, Mercurial might be the better solution.


#### C. Security

- It seems strange, but you could argue that Git and Mercurial are each more secure than the other, and you wouldn't be contradicting yourself. Which is to say, neither of them offers the security most teams need. Security in Git vs. Mercurial depends on your level of technical expertise. Mercurial may be safer for less experienced users, but Git can offer ways to enhance safety (once you know what you are doing).


#### D. Branching

- Branching means working with files — source code — that you want to modify. Branches allow you to work on different versions of your code at the same time. Then developers can merge the changes, without (hopefully) breaking the code base. One of the main reasons developers swear by Git is its effective branching model. In Git, branches are only references to a certain commit. This makes them lightweight yet powerful. Git allows you to create, delete, and change a branch anytime, without affecting the commits. If you need to test a new feature or find a bug — make a branch, do the changes, and then delete the branch.


#### E. General Conclusions

- Fundamentally, Git and Mercurial are similar, and each has its merits as a VCS. They were just designed differently and require different levels of expertise.
Git has become an industry standard, which means more developers are familiar it. Mercurial's big advantage is that it’s easy to learn and use, which is useful for less-technical content contributors.
In the Git vs. Mercurial debate, Git is the clear choice for pure web and mobile-application development. But if you have mixed assets, large global teams, or stringent security requirements, you may want to consider another alternative.
