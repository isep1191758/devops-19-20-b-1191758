package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runners.BlockJUnit4ClassRunner;
import static org.junit.jupiter.api.Assertions.*;

@RunWith(value = BlockJUnit4ClassRunner.class)

class EmployeeTest {

    @Test
    void getEmailHP() {

        //Arrange
        String expected = "123@gmail.com";
        Employee employee = new Employee("Duarte", "Barbosa", "smuggler", expected);

        //Act
        String result = employee.getEmail();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setEmailHP() {

        //Arrange//Act//Assert
        new Employee("Duarte", "Barbosa", "smuggler","teste@gmail.com");

    }
    @Test
    void setEmailNull() {

        //Arrange//Act//Assert
        assertThrows(NullPointerException.class, () -> {
            new Employee("Duarte", "Barbosa", "smuggler",null);
        });
    }

    @Test
    void setEmailEmpty() {

        //Arrange//Act//Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Duarte", "Barbosa", "smuggler", "");
        });
    }

    @Test
    void setEmailWithoutArroba() {

        //Arrange//Act//Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Duarte", "Barbosa", "smuggler", "testegmail.com");
        });
    }

    @DisplayName("create email - without .com")
    @Test
    void setIsEmailValidWithoutDot() {

        //Arrange//Act//Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("Duarte", "Barbosa", "smuggler", "teste@gmailcom");
        });
    }
    @Test
    void getFirstNameHP() {

        //Arrange
        String expected = "Duarte";
        Employee employee = new Employee(expected, "Barbosa", "smuggler", "123@gmail.com");

        //Act
        String result = employee.getFirstName();

        //Assert
        assertEquals(expected, result);
    }

    @Test
    void setFistNameHP() {

        //Arrange//Act//Assert
            new Employee("Duarte", "Barbosa", "smuggler","teste@gmail.com");
    }

    @Test
    void setFirstNameNull() {

        //Arrange//Act//Assert
        assertThrows(NullPointerException.class, () -> {
            new Employee(null, "Barbosa", "smuggler","teste@gmail.com");
        });
    }

    @Test
    void setFirstNameEmpty() {

        //Arrange//Act//Assert
        assertThrows(IllegalArgumentException.class, () -> {
            new Employee("", "Barbosa", "smuggler", "teste@gmail.com");
        });
    }


}