    Duarte Rothes Barbosa,
    1191758@isep.ipp.pt

#  Class Assignment 5.1, Turma B, Grupo 5

    26-05-2020
 
##  Demonstração através de Jenkins, do exercício gradle-basic-demo
 
 
# Objetivos/Requisitos
 
 
 
# Objetivo Principal:


  Usar a ferramenta Jenkins para efetuar as mesmas operações de exercícios anteriores, nomeadamente em CA2-part1, reproduzir o exercicio gradle-basic-demo.
 
 
# Requisitos:

1.	Instalar o programa Jenkins através do download do ficheiro jenkins.war, do site https://www.jenkins.io/doc/book/installing
2.	Através do comando em linha de comandos Windows (LCW), 'java -jar jenkins.war' no diretório onde consta o ficheiro descarregado, jenkins.war
3.	Na continuação, instalar o Jenkins por default, ie, aceitar todas as especificações como estão, e criar um conjunto de credenciais para acesso pessoal ao programa.
4.	Efetuar uma tag com a designação 'CA5-part1', na parte final
5.	Descrever o processo tomado num ficheiro readme.md.
 
 
 
# A. Início
 
 
 O primeiro passo é criar uma pasta nova para este Assignement com o nome CA5-part1, localizada na raiz, e efetuar um commit através de:

    ->   git add CA5-part1
    ->   git commit -m "CA5-part1 folder creation"
    ->   git push
 


 
# B. Construção da Pipeline
 
1.	Primeiramente, efetuar a instalação do programa Jenkins como consta nos pontos 1 e 2 dos #Requisitos;
 
2.	Na página principal do Jenkins, selecionar "Novo Item" e selecionar "Pipeline" com o nome "CA2-part1". Nessa secção selecionar através de drop-down "Pipeline script from SCM";
3. No campo Repositórios, adicionar o URL do meu 'https://isep1191758@bitbucket.org/isep1191758/devops-19-20-b-1191758.git' (repositório Bitbucket), e adicionar as credenciais de acesso com o ID 'drb devops-bitbucket';
4. No Script path, indicar:

       CA2-part1/Jenkinsfile
       

# C. Criação do ficheiro Jenkinsfile


1.	Após a abertura de um ficheiro Notepad, criar  Jenkinsfile segundo os requisitos de CA5-part1;
2.	No caminho C:\Users\ASUS\.jenkins\workspace\CA2-part1\CA2-part1, gravar o ficheiro criado acima com o nome "Jenkinsfile";
3.	Eis o conteudo do ficheiro:






        pipeline {  
            agent any  
  
         stages {  
            stage('Checkout') {  
                 steps {  
                     echo 'Checking out...'  
                     git credentialsId:'drb devops-bitbucket', url:'https://isep1191758@bitbucket.org/isep1191758/devops-19-20-b-1191758.git'  
            }  
        }  
            stage('Assemble') {  
                 steps {  
                 echo 'Assembling...'  
                 script {  
                     if (isUnix())  
                        sh './gradlew assemble'  
                     else  
                        bat 'cd CA2-part1 & gradle clean assemble'  
                }  
            }  
        }  
            stage('Test') {  
                steps {  
                echo 'Testing...'  
                script {  
                    if (isUnix())  
                        sh './gradlew test'  
                    else  
                        bat 'cd CA2-part1 & gradle test'  
                        junit ' CA2-part1/build/test-results/test/*.xml'  
                }  
            }  
        }  
             stage('Archiving') {  
                    steps {  
                    echo 'Archiving...'  
                       archiveArtifacts 'CA2-part1/build/distributions/*'  
                  }  
              }  
           }  
        }
        
        
        
        
        
4. A primeira etapa é para efetuar o checkout, ie, a origem do código de origem do repositório Bitbucket. O ID das credenciais que foram criadas previamente, foram usadas para garantir o acesso ao repositório através do seu URL, citado acima;

5. Para compilar e originar os ficheiros de arquivo, foi usado `cd CA2-part1 & gradle clean assemble` na etapa "Assemble";

6. Na fase de Testes, para executar os testes unitários e publicá-los no Jenkins foi usado o comando `cd CA2-part1 & gradle test` , sendo seguido de `CA2-part1/build/test-results/test/*.xml`, para os testes JUnit;

7. Finalmente, a etapa "Archive" grava os ficheiros gerados na fase Assemble no diretorio 'CA2-part1/build/distributions', nomeadamente:

                                                 basic_demo-0.1.0.zip, e 
                                                 basic_demo-0.1.0.tar


# D.    Ajustes Finais


Finalmente, marcar o meu repositório bitbucket com a TAG, depois dos commits inerentes testemunhando a construção do Jenkinsfile:

                   git tag CA5-part1
                   git push origin master CA5-part1.