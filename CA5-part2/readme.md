  Duarte Rothes Barbosa,
  1191758@isep.ipp.pt

#  Class Assignment 5.2, Turma B, Grupo 5

26-05-2020
 
##  Demonstração através de Jenkins, do exercício Spring-boot-application (CA2-part2)
 
 
# Objetivos/Requisitos
 
 
 
# Objetivo Principal:

Usar a ferramenta Jenkins para efetuar as mesmas operações de exercícios anteriores, nomeadamente em CA2-part2, reproduzir o spring-boot-application.
 
 
# Requisitos:

1.	Instalar o programa Jenkins através do download do ficheiro jenkins.war, do site https://www.jenkins.io/doc/book/installing
2.	Através do comando em linha de comandos Windows (LCW), 'java -jar jenkins.war' no diretório onde consta o ficheiro descarregado, jenkins.war
3.	Na continuação, instalar o Jenkins por default, ie, aceitar todas as especificações como estão, e criar um conjunto de credenciais para acesso pessoal ao programa.
4.	Efetuar uma tag com a designação 'CA5-part2', na parte final
5.	Descrever o processo tomado num ficheiro readme.md.
 
 
# A. Início
 
 O primeiro passo é criar uma pasta nova para este Assignement com o nome CA5-part2, localizada na raiz, e efetuar um commit através de:

    ->   git add CA5-part2
    ->   git commit -m "CA5-part2 folder creation"
    ->   git push
 

 
# B. Construção da Pipeline
 
1.	Na página principal do Jenkins, selecionar "Novo Item" e selecionar "Pipeline" com o nome "CA2-part2". Nessa secção selecionar através de drop-down "Pipeline script from SCM";
3. No campo Repositórios, adicionar o URL do meu 'https://isep1191758@bitbucket.org/isep1191758/devops-19-20-b-1191758.git' (repositório Bitbucket), e adicionar as credenciais de acesso com o ID 'drb devops-bitbucket';
4. No Script path, indicar:

       CA2-part2/Jenkinsfile
       

# C. Criação do ficheiro Jenkinsfile


1.	Após a abertura de um ficheiro Notepad, criar o Jenkinsfile segundo os requisitos de CA5-part2;
2.	No caminho C:\Users\ASUS\.jenkins\workspace\CA2-part2\CA2-part2, gravar o ficheiro criado acima com o nome "Jenkinsfile" (sem extensão .txt);
3.	Eis o conteudo do ficheiro:

        pipeline {  
              agent any  
  
        stages {  
        stage('Checkout') {  
            steps {  
                echo 'Checking out...'  
                git credentialsId:'drb devops-bitbucket', url:'https://isep1191758@bitbucket.org/isep1191758/devops-19-20-b-1191758.git'  
            }  
        }  
        stage('Assemble') {  
            steps {  
                echo 'Assembling...'  
                bat 'cd CA2-part2 & gradle clean assemble'  
            }  
        }  
        stage('Test') {  
            steps {  
                echo 'Testing...'  
                bat 'cd CA2-part2 & gradlew test'  
            }  
        }  
        stage('Javadoc') {  
            steps {  
                echo 'Generating Javadoc...'  
                bat 'cd CA2-part2 & gradlew javadoc'  
            publishHTML([allowMissing: false, alwaysLinkToLastBuild: false, keepAll: false, reportDir:'CA2-part2/build/docs/javadoc', reportFiles: 'index.html', reportName: 'HTML Report', reportTitles: ''])  
            }  
        }  
        stage('Archive') {  
            steps {  
                echo 'Archiving...'  
                archiveArtifacts 'CA2-part2/build/libs/*'  
            }  
        }  
        stage('Publish Image') {
            steps {
                script {
                echo 'Publishing...'
                    docker.build("0b775834563a/ca5_part2:${env.BUILD_ID}", "CA2-part2").push()
                  }
               }
            } 
          }  
       }
  
  
  
  
  
4. A primeira etapa é para efetuar o checkout, ie, a origem do código de origem do repositório Bitbucket. O ID das credenciais que foram criadas previamente, foram usadas para garantir o acesso ao repositório através do seu URL, citado acima;
5. Para compilar e originar os ficheiros de arquivo, foi usado `cd CA2-part2 & gradle clean assemble` na etapa "Assemble";
6. Na fase de Testes, para executar os testes e publicá-los no Jenkins foi usado o comando `cd CA2-part2 & gradle test` ;
7. A etapa "Javadoc" grava os ficheiros gerados na fase Assemble no diretorio 'CA2-part2/build/docs', nomeadamente, toda a documentação Javadoc (através da instalação Javadoc plugin no Jenkins), que retrata a                                                                execução do Jenkins, e todo o resultado das stages, até ao sucesso final da sua implementação, e efetuada através do comando publishHTML, que os publica no Jenkins para posterior consulta, validação e análise;
8. A etapa "Archive" grava tambem o ficheiro gerado no seu desenvolvimento, demo-0.0.1-SNAPSHOT.war, que será copiado futuramente para dentro do container que abarcará a imagem final do programa que "corre" com sucesso o spring-boot.
                                                 

# D. Edição do Dockerfile (web)

A edição do Dockerfile no path C:\Users\ASUS\.jenkins\workspace\CA2-part2\CA2-part2, surge na necessidade de cumprir os requisitos deste Assignement, nomeadamente criar e publicar uma imagem Tomcat do programa no Jenkins. Eis o ficheiro Dockerfile (adaptado do Dockerfile Web de CA4):

FROM tomcat  
  
RUN apt-get update -y  
  
RUN apt-get install -f  
  
RUN apt-get install git -y  
  
RUN apt-get install nodejs -y  
  
RUN apt-get install npm -y  
  
RUN mkdir -p /tmp/build  
  
WORKDIR /tmp/build/  
  
RUN git clone https://isep1191758@bitbucket.org/isep1191758/devops-19-20-b-1191758.git  
  
WORKDIR /tmp/build/devops-19-20-b-1191758/CA2-part2  
  
RUN chmod +x gradlew  
  
RUN ./gradlew clean build  
  
RUN cp build/libs/demo-0.0.1-SNAPSHOT.war /usr/local/tomcat/webapps/  
  
EXPOSE 8080

Ressalto o facto de na penultima linha RUN cp, se proceder à cópia do ficheiro .war gerado para depois ser incluido na imagem final, e esta ser publicada no Docker Hub.


# E. Conclusão

Para verificar o sucesso da operação, e do bom deployment no Jenkins do programa adaptado, favor verificar no path do meu repositório url: https://isep1191758@bitbucket.org/isep1191758/devops-19-20-b-1191758.git, em CA2-part2\build\docs\javadoc, os resultados desta operação.

A publicação da imagem gerada por este Pipeline pode ser verificada através do URL: https://hub.docker.com/repository/registry-1.docker.io/0b775834563a/ca5_part2/tags?page=1



# F.    Ajustes Finais

Finalmente, marcar o meu repositório bitbucket com a TAG, depois dos commits inerentes testemunhando a construção do Jenkinsfile, e do Dockerfile:

                   git tag CA5-part2(final)
                   git push origin master CA5-part2(final).